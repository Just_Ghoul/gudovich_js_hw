/*Завдання №1*/
const a1 = ["a","b","c",];
const b1 = [1,2,3,];
let c1 = a1.concat(b1);
document.write(`<h3>Завдання №1</h3>
<p>${c1.join(", ")}</p>`);

/*Завдання №2*/
const a2 = ["a","b","c",];
a2.push(1, 2, 3.);
document.write(`<h3>Завдання №2</h3>
<p>${a2.join(", ")}</p>`);

/*Завдання №3*/
const a3 = ["1","2","3",];
a3.reverse();
document.write(`<h3>Завдання №3</h3>
<p>${a3.join(", ")}</p>`);

/*Завдання №4*/
const a4 = [1, 2, 3];
a4.push(4, 5, 6,);
document.write(`<h3>Завдання №4</h3>
<p>${a4.join(", ")}</p>`);

/*Завдання №5*/
const a5 = [1, 2, 3,];
a5.unshift(4, 5, 6,);
document.write(`<h3>Завдання №5</h3>
<p>${a5.join(", ")}</p>`);

/*Завдання №6*/
const a6 = ['js', 'css', 'jq',];
document.write(`<h3>Завдання №6</h3>
<p>Данно масив ['js', 'css', 'jq',]</p>
<p>Перший елемент масиву: ${a6[0]}</p>`);

/*Завдання №7*/
const a7 = [1, 2, 3, 4, 5];
const b7 = a7.slice(0, 3);
document.write(`<h3>Завдання №7</h3>
<p>${b7.join(", ")}</p>`);

/*Завдання №8*/
const a8 = [1, 2, 3, 4, 5];
const b8 = a8.splice(1, 2);
document.write(`<h3>Завдання №8</h3>
<p>${a8.join(", ")}</p>`);

/*Завдання №9*/
const a9 = [1, 2, 3, 4, 5];
const b9 = a9.splice(2, 0, 10);
document.write(`<h3>Завдання №9</h3>
<p>${a9.join(", ")}</p>`);

/*Завдання №10*/
const a10 = [3, 4, 1, 2, 7];
a10.sort();
document.write(`<h3>Завдання №10</h3>
<p>${a10.join(", ")}</p>`);

/*Завдання №11*/
const a11 = ['Привіт',', ', 'світ', '!'];
a11[2] = "мир";
document.write(`<h3>Завдання №11</h3>
<p>${a11.join("")}</p>`);

/*Завдання №12*/
const a12 = ['Привіт',', ','світ','!'];
a12[0] = "Поки";
document.write(`<h3>Завдання №12</h3>
<p>${a12.join("")}</p>`);

/*Завдання #13*/
const arr1 = [1, 2, 3, 4, 5];
const arr2 = new Array(1, 2, 3, 4, 5);
document.write(`<h3>Завдання №13</h3>
<p>Перший массив: ${arr1.join(", ")}</p>
<p>Другий массив: ${arr2.join(", ")}</p>`);

/*Завдання №14*/
var arr = [['блакитний', 'червоний', 'зелений'],['blue', 'red', 'green'],];
document.write(`<h3>Завдання №14</h3>
<p>'${arr[0][0]}' '${arr[1][0]}'</p>`);

/*Завдання №15*/
const arr15 = ['a','b','c','d'];
document.write(`<h3>Завдання №15</h3>
<p>'${arr15[0]} + ${arr15[1]}, ${arr15[2]} + ${arr15[3]}'</p>`);

/*Завдання №16*/
document.write(`<h3>Завдання №16</h3>`);
let qIndex = parseInt(prompt("Введіть кількість індексів для массиву.", "5"));
const arr16 = [];
for(let i = 0; i < qIndex; i++){
    arr16.push(i)
}
document.write(`${arr16.join("</br>")}`);

/*Завдання №17*/
const arrEven = [];
const arrOdd = [];
for(let j = 0; j < arr16.length; j++){
    if(j % 2 == 1)
    arrOdd.push(j)
    else
    arrEven.push(j)
}
document.write(`<h3>Завдання №17</h3>
<p>${arrOdd.join(", ")}</p>
<span style="background: red;">${arrEven.join(", ")}</span>`)

/*Завдання №18*/
const vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
vegetables[3] = "Морквина";
document.write(`<h3>Завдання №18</h3>
<p>"${vegetables.join(", ")}"</p>`);