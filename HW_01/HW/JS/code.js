var x = 6;
var y = 14;
var z = 4;
let result1 = x+=y-x++*z;
document.write(`<p>Результат першого виразу: ${result1}</p>
Спочатку виконується х++, що додає до нього 1, але так як цей инкремент постфіксний, то він додається для подальших операцій і поки що х = 6, потім працює унарний мінус, який робить з 6, -6, після чого -х помножується на z, та виходить -24, після цього працює складання y, яке рівняється 14, та х, яке рівняється -24, та виходить - 10, в самому кінці присвоюється зі складанням -10 до х, яке є 6, та виходить результат -4`);
var x = 6;
var y = 14;
var z = 4;
let result2 = z = -- x - y *5;
document.write(`<p>Результат другого виразу: ${result2}</p>
Спочатку виконується Префіксний декремент до х, та х стає 5, потім y з унарним мінусом, помножується на 5, та стає -70, після цього, проводиться складання -70, та 5, в результаті виходить -65, що присвоюється до z`);
var x = 6;
var y = 14;
var z = 4;
let result3 = y /= x + 5 % z;
document.write(`<p>Результат третього виразу: ${result3}</p>
Спочатку виконується ділення з остатком 5 на 4, що дає в результаті 1, після цього складається х, який рівен 6, з цим залишком від ділення, та виходить 7, в кінці операції, y, який має значення 14, ділиться на 7, та це значення присвоюється до y, в результаті залишається 2`);
var x = 6;
var y = 14;
var z = 4;
let result4 = z - x ++ + y * 5;
document.write(`<p>Результат четвертого виразу: ${result4}</p>
Спочатку виконується постфіксний інкремент до х, що дає для цієї операції 6, потім множення y на 5, що дає 70, потім складання 70 та -6, тому що унарний мінус впливає на х, тому результатом складання є 64, та потім вже йде складання 64, з z, яка становить 4, в результаті отримуємо 68`);
var x = 6;
var y = 14;
var z = 4;
let result5 = x = y - x ++ *z;
document.write(`<p>Результат п'ятого виразу: ${result5}</p>
Спочатку виконується постфіксний інкремент до х, та х залишається 6, потім унарний мінус впливає на х, та робить його -6, потім -х помножується на z, що в результаті дає -24, та потім складає -24, та y, що дає -10, та в кінці, присвоює -10 до х`);