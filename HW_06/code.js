class Worker {
    constructor(name, sname, r, d){
    this.name = name;
    this.surname = sname;
    this.rate = r;
    this. days = d;
    }
    getSalary() {
        return this.rate * this.days;
    };
}
const user = new Worker('Ivan', 'Petrov', 30, 23);
document.write(`Hi, ${user.name}  ${user.surname}, your salary for this month is ${user.getSalary()}`);

class MyString {
    constructor(str){
        this.string = str;
    }

    reverse(str){
        return str.reverse().join('');
    };
    ucFirst(str){
        return str[0].toUpperCase() + str.slice(1);
    };
    ucWord(str){
        for(let i = 0; i <str.length; i++){
            str[i] = str[i][0].toUpperCase() + str[i].slice(1); 
        }
        return str.join(' ');
    }

}
const str = new MyString('hi there, my name is Mykola!');
console.log(str.reverse([...str.string]));
console.log(str.ucFirst(str.string));
console.log(str.ucWord(str.string.split(' ')));


class Phone {
    constructor(num, mod, wg){
        this.number = num;
        this.model  = mod;
        this.weight = wg;
    }
    receiveCall(name){
        console.log(`Телефонує ${name}`);
    }
    getNumber(){
       return this.number;
    }
}
const ph1 = new Phone(380952156783, 'Nokia', 400);
const ph2 = new Phone(380952137812, 'Iphone', 180);
const ph3 = new Phone(380952115601, 'Samsung', 176);

ph1.receiveCall('Petro');
console.log(ph1.getNumber());
ph2.receiveCall('Sergiy');
console.log(ph2.getNumber());
ph3.receiveCall('Mykola');
console.log(ph3.getNumber());


class Driver {
    constructor(name, sname, exp){
        this.name = name;
        this.surname = sname;
        this.experience = exp;
    }
    toString(){
        return (`Водій ${this.name} ${this.surname}, з досвідом водіння ${this.experience} років.`)
    }
}
class Engine {
    constructor(power, company){
        this.power = power;
        this.company = company;
    }
    toString(){
        return (`Двигун марки ${this.company}, потужністтю: ${this.power}к/с.`)
    }
}
class Car {
    constructor(clas, mod,  wg, eng, driv){
        this.carClass = clas;
        this.model = mod;
        this.weight = wg;
        this.engine = eng;
        this.driver = driv;
    }
    start (){
        document.write(`Поїхали`);
    }
    stop (){
        document.write(`Зупиняємось`);
    }
    turnLeft (){
        document.write(`Повертаємо ліворуч`);
    }
    turnRight (){
        document.write(`Повертаємо праворуч`);
    }
    toStringAll(){
        return (`<p>Автомобіль марки ${this.model}, ${this.carClass} классу</p>
                        <p>Вага: ${this.weight} кг.</p>
                        <p>${this.engine.toString()}</p>
                        <p>${this.driver.toString()}</p>`)
    }
}   
class Lorry extends Car {
    constructor(clas, mod, wg, eng, driv, carry){
        super(clas, mod, wg, eng, driv);
        this.carrying = carry;
    }
    toString(){
        return (`<p>Вантажопідйомність автомобілю складає: ${this.carrying} кг.</p>`)
    }
}
class SportCar extends Car {
    constructor(clas, mod, wg, eng, driv, speed){
        super(clas, mod, wg, eng, driv);
        this.maxSpeed = speed;
    }
    toString(){
        return (`<p>Максимальна швидкість автомобілю складає: ${this.maxSpeed} км/г.</p>`)
    }
}
const driver1 = new Driver('Ivan', 'Petrov', 15);
const engine1 = new Engine(360, 'Ford');
const lorry1 = new Lorry('Minivan', 'Ford Transit', 1800, engine1, driver1, 1100);
document.write(lorry1.toStringAll(), lorry1.toString());
const driver2 = new Driver('Michael', 'Shumacher', 30);
const engine2 = new Engine(760, 'AMG');
const sport1 = new SportCar('Supercar', 'Pagani Zonda R', 1050, engine2, driver2, 390)
document.write(sport1.toStringAll(), sport1.toString());


class Animal {
    constructor(food, location){
        this.food = food;
        this.location = location;
    }
    makeNoise(){
        return `Створює шум!`
    }
    eat(){
        return `Їсть`
    }
    sleep(){
        console.log(`Ця тварина спить!`)
    }
}
class Cat extends Animal{
    constructor(food, location, scratch){
        super(food, location);
        this.scratch = scratch;
    }
    makeNoise(){
        return `Не шумить!`
    }
    eat(){
        return `Не їсть`
    }
}
class Dog extends Animal{
    constructor(food, location, bark){
        super(food, location);
        this.bark = bark;
    }
    makeNoise(){
        return `Шумить!`
    }
    eat(){
        return `Не їсть`
    }
}
class Hourse extends Animal{
    constructor(food, location, mane){
        super(food, location);
        this.mane = mane;
    }
    makeNoise(){
        return `Не шумить!`
    }
    eat(){
        return `Їсть`
    }
}
class Doctor{
    constructor(){ 
    }
    treatAnimal(animal){
        document.write(`<p>Їжа: ${animal.food} </br> Місцезнаходження: ${animal.location}</p>`)
    }
    main(Animal){
        for(let key of Animal){
            this.treatAnimal(key)
        }
    }
}
const cat1 = new Cat('Риба', 'Диван', false);
const dog1 = new Dog('Мʼясо', 'Подвірʼя', true);
const hourse1 = new Hourse('Сіно', 'Конюшня', true);
const treat = new Doctor();
treat.main([cat1, dog1, hourse1]);